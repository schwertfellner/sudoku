{-# LANGUAGE OverloadedStrings, TemplateHaskell #-} --enables Hotkeys but at the cost that every normal String has to be called with pack (Solution from: https://catherineh.github.io/programming/2016/12/25/haskell-anxiety-3)
module Main where

import Graphics.UI.Gtk.Gdk.EventM
import Graphics.UI.Gtk as Gtk
import DataTypes
import Control.Monad
import Logic
import Control.Monad.IO.Class
import Data.Text hiding (length, map, reverse)
import Control.Lens as Lens
import System.IO.Unsafe
import Data.List

makeLenses ''CustomButton

-- | main function
-- most functionality of the gui is in here
main :: IO ()
main = do
    initGUI
    window <- windowNew

    containerBox <- vBoxNew False 0

    menuBar <- menuBarNew


    menuSettings <- menuItemNewWithLabel (pack "Settings")

    settingsChildren <- menuNew

    newGame <- menuItemNewWithLabel (pack "New Game")

    newGameChildren <- menuNew

    newGrid <- menuItemNewWithLabel (pack "New Grid")

    openGame <- menuItemNewWithLabel (pack "Open Game")
    openRandomGame <- menuItemNewWithLabel (pack "Open Random Game")

    saveTemplate <- menuItemNewWithLabel (pack "Save Template")
    trennlinie <- separatorMenuItemNew
    solveGame <- menuItemNewWithLabel (pack "Solve Game")

    menuHelp <- menuItemNewWithLabel (pack "Help")

    helpChildren <- menuNew

    sodukoRules <- menuItemNewWithLabel (pack "Soduko Rules")
    howToPlay <- menuItemNewWithLabel (pack "How To Play")
    credits <- menuItemNewWithLabel (pack "Credits")

    menuExit <- menuItemNewWithLabel (pack "Exit")

    infoLabel <- labelNew (Just (pack "info"))

    grid <- tableNew 0 0 True
    gridAlignment <- frameNew
    tableSetRowSpacings grid 5
    tableSetColSpacings grid 5
    widgetModifyBg gridAlignment StateNormal (Color 10000 10000 10000)

    box1 <- tableNew 3 3 True
    box1buttons <- replicateM 9 toggleButtonNew
    let putChildrenInTable [] _ = return ()
        putChildrenInTable l@(a:as) box = do
            let x = 2 - (mod (9 - (length l)) 3)
            let y = if (length l) < 4 then 1 else if (length l) > 6 then 3 else 2
            tableAttachDefaults box a x (x+1) (y-1) y
            putChildrenInTable as box
    putChildrenInTable box1buttons box1
    box1boxes <- replicateM 9 $ hBoxNew False 0
    let putLabelsInButton [] _ = return ()
        putLabelsInButton (a:as) (b:bs) = do
            Gtk.set b [containerChild := a]
            putLabelsInButton as bs
    putLabelsInButton box1boxes box1buttons
    box1labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    let changeLabelText [] _ = return ()
        changeLabelText l@(x:xs) string = do
            Gtk.set x [labelLabel := string]
            changeLabelText xs string
    putLabelsInButton box1labels box1boxes
    box1penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box1penGrid box1boxes
    box1penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box1penLabels box1penGrid))
    tableSetRowSpacings box1 2
    tableSetColSpacings box1 2


    let box1ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box1buttons box1labels) box1penLabels)


    box2 <- tableNew 3 3 True
    box2buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box2buttons box2
    box2boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box2boxes box2buttons
    box2labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box2labels box2boxes
    box2penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box2penGrid box2boxes
    box2penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box2penLabels box2penGrid))
    tableSetRowSpacings box2 2
    tableSetColSpacings box2 2


    let box2ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box2buttons box2labels) box2penLabels)


    box3 <- tableNew 3 3 True
    box3buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box3buttons box3
    box3boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box3boxes box3buttons
    box3labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box3labels box3boxes
    box3penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box3penGrid box3boxes
    box3penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box3penLabels box3penGrid))
    tableSetRowSpacings box3 2
    tableSetColSpacings box3 2


    let box3ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box3buttons box3labels) box3penLabels)


    box4 <- tableNew 3 3 True
    box4buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box4buttons box4
    box4boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box4boxes box4buttons
    box4labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box4labels box4boxes
    box4penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box4penGrid box4boxes
    box4penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box4penLabels box4penGrid))
    tableSetRowSpacings box4 2
    tableSetColSpacings box4 2

    let box4ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box4buttons box4labels) box4penLabels)


    box5 <- tableNew 3 3 True
    box5buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box5buttons box5
    box5boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box5boxes box5buttons
    box5labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box5labels box5boxes
    box5penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box5penGrid box5boxes
    box5penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box5penLabels box5penGrid))
    tableSetRowSpacings box5 2
    tableSetColSpacings box5 2


    let box5ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box5buttons box5labels) box5penLabels)


    box6 <- tableNew 3 3 True
    box6buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box6buttons box6
    box6boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box6boxes box6buttons
    box6labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box6labels box6boxes
    box6penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box6penGrid box6boxes
    box6penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box6penLabels box6penGrid))
    tableSetRowSpacings box6 2
    tableSetColSpacings box6 2


    let box6ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box6buttons box6labels) box6penLabels)


    box7 <- tableNew 3 3 True
    box7buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box7buttons box7
    box7boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box7boxes box7buttons
    box7labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box7labels box7boxes
    box7penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box7penGrid box7boxes
    box7penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box7penLabels box7penGrid))
    tableSetRowSpacings box7 2
    tableSetColSpacings box7 2


    let box7ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box7buttons box7labels) box7penLabels)

    box8 <- tableNew 3 3 True
    box8buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box8buttons box8
    box8boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box8boxes box8buttons
    box8labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box8labels box8boxes
    box8penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box8penGrid box8boxes
    box8penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box8penLabels box8penGrid))
    tableSetRowSpacings box8 2
    tableSetColSpacings box8 2


    let box8ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box8buttons box8labels) box8penLabels)

    box9 <- tableNew 3 3 True
    box9buttons <- replicateM 9 toggleButtonNew
    putChildrenInTable box9buttons box9
    box9boxes <- replicateM 9 $ hBoxNew False 0
    putLabelsInButton box9boxes box9buttons
    box9labels <- replicateM 9 $ labelNew (Nothing::Maybe String)
    putLabelsInButton box9labels box9boxes
    box9penGrid <- replicateM 9 $ tableNew 3 3 True
    putLabelsInButton box9penGrid box9boxes
    box9penLabels <- replicateM 9 $ replicateM 9 $ labelNew (Nothing::Maybe String)
    sequence_ (map (\(x,y) -> putChildrenInTable x y) (Prelude.zip box9penLabels box9penGrid))
    tableSetRowSpacings box9 2
    tableSetColSpacings box9 2
    widgetSetSizeRequest box1 200 200

    let box9ButtonsDataType = map (\((b,n),p) -> Button {_button = b, _numberLabel = n, _pencLabels = p, _normalNumberMode = True}) (Prelude.zip (Prelude.zip box9buttons box9labels) box9penLabels)
    
    font <- fontDescriptionNew
    --fontDescriptionSetSize (box1labels !! 0) 20.0
    --fontDescriptionSetSize font 20.0
    labelSetMarkup infoLabel ("<big><big><big>info</big></big></big>"::String)



    let boxes = [box9, box8, box7, box6, box5, box4, box3, box2, box1]
    let buttonsSolo = box9buttons ++ box8buttons ++ box7buttons ++ box6buttons ++ box5buttons ++ box4buttons ++ box3buttons ++ box2buttons ++ box1buttons
    let buttons = Prelude.zip buttonsSolo [1..81]
    let buttonLabels = box9labels ++ box8labels ++ box7labels ++ box6labels ++ box5labels ++ box4labels ++ box3labels ++ box2labels ++ box1labels
    let boxPenGrid = box9penGrid ++ box8penGrid ++ box7penGrid ++ box6penGrid ++ box5penGrid ++ box4penGrid ++ box3penGrid ++ box2penGrid ++ box1penGrid
    let boxPenLabels = box9penLabels ++ box8penLabels ++ box7penLabels ++ box6penLabels ++ box5penLabels ++ box4penLabels ++ box3penLabels ++ box2penLabels ++ box1penLabels

    let tripleButtonLabelLabel = zip3 buttonsSolo buttonLabels boxPenLabels
    let tripleButtonLabelBox = map (\((a,b,c),d) -> (a,b,(c,d))) $ Prelude.zip tripleButtonLabelLabel boxPenGrid

    putChildrenInTable boxes grid
    
    -- undo/pencilmark/redo
    boxIcon <- hBoxNew True 50 -- homogeneous, spacing

    undoButton <- buttonNew
    undoImage <- imageNewFromFile "C:/Users/Benji/Documents/ffp/app/sudoku/resources/undo-small.png"
    pencilButton <- toggleButtonNew -- toggleButtonNewWithLabel "string"
    pencilImage <- imageNewFromFile "C:/Users/Benji/Documents/ffp/app/sudoku/resources/pencil-small.png"
    redoButton <- buttonNew
    redoImage <- imageNewFromFile "C:/Users/Benji/Documents/ffp/app/sudoku/resources/redo-small.png"

    -- setting connections
    Gtk.set window [windowTitle := (pack "Sudoku"), 
        containerChild := containerBox,
        windowDefaultWidth := 580,
        windowDefaultHeight := 630]
    boxPackStart containerBox menuBar PackNatural 0
    Gtk.set containerBox [containerChild := gridAlignment]
    
    boxPackStart containerBox boxIcon PackNatural 0
    boxPackStart containerBox infoLabel PackNatural 0
    Gtk.set menuBar [containerChild := menuSettings, containerChild := menuHelp, containerChild := menuExit]
    Gtk.set menuSettings [menuItemSubmenu := settingsChildren]
    Gtk.set settingsChildren [containerChild := newGame, containerChild := saveTemplate, containerChild := trennlinie, containerChild := solveGame]
    Gtk.set newGame [menuItemSubmenu := newGameChildren]
    Gtk.set newGameChildren [containerChild := newGrid, containerChild := openGame, containerChild := openRandomGame]
    Gtk.set menuHelp [menuItemSubmenu := helpChildren]
    Gtk.set helpChildren [containerChild := sodukoRules, containerChild := howToPlay, containerChild := credits]
    Gtk.set boxIcon [containerChild := undoButton, containerChild := pencilButton, containerChild := redoButton]
    Gtk.set undoButton [buttonImage := undoImage]
    Gtk.set pencilButton [buttonImage := pencilImage]
    Gtk.set redoButton [buttonImage := redoImage]
    Gtk.set gridAlignment [containerChild := grid, containerBorderWidth := 10]
    Gtk.set grid [containerBorderWidth := 5]


    sequence_ [on newGrid menuItemActivate $ newGridFunction infoLabel n buttonLabels boxPenGrid | n <- (buttonLabels : (boxPenLabels))]

    sequence_ [on (fst b) buttonActivated $ untoggleeverythingignoringpencil b buttons | b <- buttons]

    sequence_ (map (\b -> Gtk.set b [widgetNoShowAll := True]) buttonLabels)

    sequence_ [on openGame menuItemActivate $ Gtk.set infoLabel [labelLabel := pack "not yet implemented"] | item <- [openGame, openRandomGame, sodukoRules, howToPlay, credits]]

    on saveTemplate menuItemActivate $ saveString $ getGridContent buttonLabels

    --on solveGame menuItemActivate $ print $ findHiddenInRCB $ findNakedSingles $ findHiddenInRCB $ findNakedSingles $ initalGridFilter $ translateStringToGrid (getGridContent buttonLabels)--setGridContent (decodeSudoku (getGridContent buttonLabels)) buttonLabels

    on solveGame menuItemActivate $ setGridContent (lineNotationToBox (reverse (testDecode (boxNotationToLine (reverse (getGridContent buttonLabels)))))) buttonLabels -- $ initalGridFilter

    --on solveGame menuItemActivate $ setGridContent (decodeSudoku (getGridContent buttonLabels)) buttonLabels

-- containerGetChildren :: ContainerClass self => self -> IO [Widget]

    sequence_ [on ((\(x,_,_) -> x) b) keyPressEvent $ f b pencilButton | b <- tripleButtonLabelBox, f <- [recognizeOne, recognizeTwo, recognizeThree, recognizeFour, recognizeFive, recognizeSix, recognizeSeven, recognizeEight, recognizeNine]]


{-    on (box1buttons !! 0) keyPressEvent $ tryEvent $ do
        "1" <- eventKeyName
        liftIO $ Gtk.set (box1labels !! 0) [labelLabel := (pack "1")]-}

{-    
    window `on` keyPressEvent $ tryEvent $ do
        "9" <- eventKeyName
        liftIO $ putStrLn "9"-}

    on window objectDestroy mainQuit
    widgetShowAll window
    mainGUI

-- | used for untoggling the other buttons when pressing one on the grid
-- ignores the pencilmark button as that should stay as it was
untoggleeverythingignoringpencil :: (Eq b, ToggleButtonClass a, ToggleButtonClass o) => (a, b) -> [(o, b)] -> IO ()
untoggleeverythingignoringpencil b l@(x:xs) = do
    t <- toggleButtonGetActive (fst b)
    if not $ t then return () else help b l where
        help b [] = return ()
        help b (x:xs) = do
            if (snd b) == (snd x) then return () else Gtk.set (fst x) [toggleButtonActive := False]
            help b xs

-- | removes the text of every Label in the grid to reset the sudoku grid
--newGridFunction :: Label -> [Label] -> Label -> [] --Box
newGridFunction infoLabel l bigL smallB = do
    sequence_ (map (\x -> Gtk.set x [labelLabel := (pack "")]) l)
    sequence_ (map (\b -> Gtk.set b [widgetVisible := False]) bigL)
    sequence_ (map (\b -> Gtk.set b [widgetVisible := True]) smallB)
    Gtk.set infoLabel [labelLabel := (pack "<big><big><big>Empty Grid loaded!</big></big></big>")]

-- | reads all numbers from the grid (ignoring pencilmarks as those are not used by the system)
-- getGridContent :: [Label] ->
getGridContent l = intersect (Prelude.concat $ map (\x -> if (unsafePerformIO (labelGetText x)::String) == "" then "_" else (unsafePerformIO (labelGetText x)::String)) l) "_123456789"

-- | writes the numbers (received via string) into the labels
setGridContent :: String -> [Label] -> IO ()
--setGridContent string listentry = sequence_ ([Gtk.set (snd entry) (if (fst entry) == '_' then [labelLabel := (pack "")] else [labelLabel := pack [fst entry], widgetVisible := True]) | entry <- (Prelude.zip string listentry)])
{-}
setGridContent string listentry = sequence_ $ help string listentry where
    help [] _ = []
    help (x:xs) (y:ys) = case x of
        '_' -> (Gtk.set y [labelLabel := (pack "")]):(help xs ys)
        x -> (Gtk.set y [widgetVisible := True]):(labelSetMarkup y (changeLabelBig 2 [x])):(help xs ys)
-}
setGridContent string listentry = sequence_ $ help string listentry [] where
    help [] _ res = res
    help (x:xs) (y:ys) res = help xs ys $ if x == '_' then (Gtk.set y [labelLabel := (pack "")]):res else (Gtk.set y [widgetVisible := True]):(labelSetMarkup y (changeLabelBig 2 [x])):res

-- | functions for reading the number input from the user
--recognizeOne, recognizeTwo, recognizeThree, recognizeFour, recognizeFive, recognizeSix, recognizeSeven, recognizeEight, recognizeNine :: (ToggleButton, Label, (HBox, [Label])) -> IO ()
recognizeOne (b,n,(p,t)) pb = tryEvent $ do
    "1" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool)
        then if ((unsafePerformIO (labelGetText n))::String) == "1"
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "1")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 8 in if (((unsafePerformIO (labelGetText l))::String) == "1") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "1")]

recognizeTwo (b,n,(p,t)) pb = tryEvent $ do
    "2" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool)
        then if ((unsafePerformIO (labelGetText n))::String) == "2"
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "2")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 7 in if (((unsafePerformIO (labelGetText l))::String) == "2") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "2")]

recognizeThree (b,n,(p,t)) pb = tryEvent $ do
    "3" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool)
        then if ((unsafePerformIO (labelGetText n))::String) == "3"
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "3")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 6 in if (((unsafePerformIO (labelGetText l))::String) == "3") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "3")]

recognizeFour (b,n,(p,t)) pb = tryEvent $ do
    "4" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "4" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "4")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 5 in if (((unsafePerformIO (labelGetText l))::String) == "4") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "4")]

recognizeFive (b,n,(p,t)) pb = tryEvent $ do
    "5" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "5" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "5")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 4 in if (((unsafePerformIO (labelGetText l))::String) == "5") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "5")]

recognizeSix (b,n,(p,t)) pb = tryEvent $ do
    "6" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "6" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "6")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 3 in if (((unsafePerformIO (labelGetText l))::String) == "6") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "6")]

recognizeSeven (b,n,(p,t)) pb = tryEvent $ do
    "7" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "7" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "7")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 2 in if (((unsafePerformIO (labelGetText l))::String) == "7") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "7")]

recognizeEight (b,n,(p,t)) pb = tryEvent $ do
    "8" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "8" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "8")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 1 in if (((unsafePerformIO (labelGetText l))::String) == "8") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "8")]

recognizeNine (b,n,(p,t)) pb = tryEvent $ do
    "9" <- eventKeyName
    liftIO $ if not (unsafePerformIO (toggleButtonGetActive pb)::Bool) 
        then if ((unsafePerformIO (labelGetText n))::String) == "9" 
            then do
                Gtk.set n [labelLabel := (pack ""), widgetVisible := False]
                Gtk.set t [widgetVisible := True]
            else do
                Gtk.set t [widgetVisible := False]
                labelSetMarkup n (changeLabelBig 2 "9")
                Gtk.set n [widgetVisible := True]
        else
            let l = p !! 0 in if (((unsafePerformIO (labelGetText l))::String) == "9") then Gtk.set l [labelLabel := (pack "")] else Gtk.set l [labelLabel := (pack "9")]

changeLabelBig n string = Prelude.concat (Prelude.replicate n "<big>") ++ string ++ Prelude.concat (Prelude.replicate n "</big>")

checkIfEmpty label = ((unsafePerformIO (labelGetText label))::String) == ""


 --   labelSetMarkup infoLabel ("<big><big><big>info</big></big></big>"::String)