{-# LANGUAGE DeriveLift, RecordWildCards #-}
module DataTypes where

import Data.List
import Language.Haskell.TH.Syntax
--import Graphics.UI.Gtk
{-
-- | deprecated
data Space = Space {_num :: Number, _pencilmarks :: [Number], _pencilShow :: Bool} deriving Show
-}
-- | deprecated
--data CustomButton = Button { _button :: ToggleButton, _numberLabel :: Label, _pencLabels :: [Label], _normalNumberMode :: Bool} --uncomand CustomButtons and Graphics.UI.Gtk to stack build


data Found = None | FoundOne | FoundMore Int deriving Show

instance Eq Found where
    None == None = True
    FoundOne == FoundOne = True
    (FoundMore a) == (FoundMore b) = a == b
    _ == _ = False

instance Enum Found where
    toEnum n = case n of 
        0 -> None
        1 -> FoundOne
        num -> FoundMore num
    fromEnum n = case n of
        None -> 0
        FoundOne -> 1
        FoundMore num -> num
    succ = toEnum . succ . fromEnum
    pred = toEnum . pred . fromEnum    

-- | searches a list for elements that fulfill the condition
-- returns true if there is exactly one element in the list that fulfills it
exactlyOne :: (a -> Bool) -> [a] -> Bool
exactlyOne f l = FoundOne == (help f l) where
    help function [] = None
    help function (x:xs) = if function x then succ (help function xs) else (help function xs)
help2 candidates = let nums = map (\x -> (fromEnum x)::Int) candidates in map (\x -> toEnum x::Number) [ n | n <- [1..9] , exactlyOne (\x -> elem n x) (split 1 nums)]

-- | searches a list for elements that fulfill the condition
-- returns true if there are exactly n elements in the list that fulfill it
exactlyN :: Int -> (a -> Bool) -> [a] -> Bool
exactlyN n f l = FoundMore n == (help f l) where
    help function [] = None
    help function l@(x:xs) = if function x then succ (help function xs) else (help function xs)

-- | searches a list for elements that fulfill the condition
-- returns true if there are no elements in the list that fulfill it
exactlyNone :: (a -> Bool) -> [a] -> Bool
exactlyNone f l = None == (help f l) where
    help function [] = None
    help function l@(x:xs) = if function x then FoundOne else (help function xs)

-- | data type for numbers one to nine
data Number = One | Two | Three | Four | Five | Six | Seven | Eight | Nine deriving (Show, Lift)

-- | equality on Number
instance Eq Number where
    One == One = True
    Two == Two = True
    Three == Three = True
    Four == Four = True
    Five == Five = True
    Six == Six = True
    Seven == Seven = True
    Eight == Eight = True
    Nine == Nine = True
    _ == _ = False

-- | instance of Enum for Number 
instance Enum Number where
    toEnum n = [One,Two,Three,Four,Five,Six,Seven,Eight,Nine] !! (n - 1)
    fromEnum n | n == One = 1
               | n == Two = 2
               | n == Three = 3
               | n == Four = 4
               | n == Five = 5
               | n == Six = 6
               | n == Seven = 7
               | n == Eight = 8
               | n == Nine = 9
    succ Nine = One
    succ n = toEnum $ succ $ fromEnum n
    pred One = Nine
    pred n = toEnum $ pred $ fromEnum n

instance Ord Number where
    compare a b = compare (fromEnum a) (fromEnum b)

-- | addition to Enum, translates Number to Char
fromEnumToChar :: Number -> Char
fromEnumToChar = head . show . fromEnum

-- | the grid is a list of (81) fields
newtype Grid = Grid [Field] deriving (Show, Lift)

gridMap f (Grid g) = Grid (map f g)

unGrid (Grid g) = g

{- | a field is either a number or a list of pencilmarks and its position on the field

__Examples:__

@
number = Field {_number = (N Five), _position = RCB {_row = 3, _column = 5, _box = 2}}
pencil = Field {_number = (P [One, Three]), _position = RCB {_row = 9, _column = 8, _box = 9}}
@
-}
data Field = Field {
    _number :: NumberPencilBackend, -- ^ can be a number or a list of pencilmarks
    _position :: RCB -- ^ is made up of row, column and box
    } deriving (Eq, Lift)

instance Show Field where
    show (Field {_number = n, _position = RCB {_row = r, _column = c, _box = b}}) = "(" ++ show n ++ " r" ++ show r ++ "c" ++ show c ++ "b" ++ show b ++")"

-- | saves the row, column and box values
data RCB = RCB {
    _row :: Int,
    _column :: Int,
    _box :: Int
    } deriving (Show, Lift)

-- | Eq for RCB, two RCBs are equal if all their respective values are equal
instance Eq RCB where
    (RCB {_row = r1, _column = c1, _box = b1}) == (RCB {_row = r2, _column = c2, _box = b2}) = r1 == r2 && c1 == c2 && b1 == b2

-- | Enum instance for RCB
instance Enum RCB where
    toEnum count = convertCountToRCB (count-1)
    fromEnum (RCB {_row = r, _column = c, ..}) = ((r-1)*9) + c
    succ = toEnum . succ . fromEnum
    pred = toEnum . pred . fromEnum

-- | used in Enum
-- calculates the correct values from a number (1-81) on the grid
convertCountToRCB :: Int -> RCB
convertCountToRCB count | count < 81 = let {r = (div count 9) + 1; c = (mod count 9) + 1} in RCB {_row = r, _column = c, _box = (boxFromRowColumn r c)}

convertRCBToCount (RCB {_row = r, _column = c, _box = _}) = (r-1)*9 + c

-- | calculates the correct box from the row and column values
boxFromRowColumn :: Int -> Int -> Int
boxFromRowColumn r c = do
    let rowPossible = take 3 (drop ((div (r - 1) 3)*3) [1..9])
    let columnPossible = take 3 (drop ((div (c - 1) 3)*3) [1,4,7,2,5,8,3,6,9])
    head $ intersect rowPossible columnPossible

-- | shorthand for differenciating between row, column and box logic
-- used in sorting and eliminating possible numbers
data RCBEnum = Row | Column | Box deriving (Eq, Show)

{- | can either be a number N (the final number on the grid) or a pencilmark P (with a list of possible numbers)
the pencilmarks are independent from the ones the user inputs

__Examples:__

@
number = N One
pencil = P [Three .. Five]
@
-}
data NumberPencilBackend = N Number | P [Number] deriving Lift

instance Show NumberPencilBackend where
    show (N n) = "N " ++ show n
    show (P p) = "P " ++ show p

-- | equality for NumberPencilBackend
-- in case of two numbers, it checks for their equality
-- in case of two pencilmarks, it checks if the lists are equal
-- anything else is not equal
instance Eq NumberPencilBackend where
    (N n) == (N m) = n == m
    (P l1) == (P l2) = help (nub (sort l1)) (nub (sort l2)) where
        help [] [] = True
        help (x:xs) (y:ys) = if elem x ys && elem y xs then help xs ys else False
        help _ _ = False
    _ == _ = False

-- | 
sortP :: NumberPencilBackend -> NumberPencilBackend
sortP (P list) =  P (sort list)

-- | addition to Eq for NumberPencilBackend
-- checks only if two numbers of type NumberPencilBackend have the same kind (i.e N and N or P and P)
eqType :: NumberPencilBackend -> NumberPencilBackend -> Bool
eqType (N _) (N _) = True
eqType (P _) (P _) = True
eqType _ _ = False

-- | returns true if number of type NumberPencilBackend is of kind P
isP :: NumberPencilBackend -> Bool
isP (P _) = True
isP _ = False

-- | returns true if number of type NumberPencilBackend is of kind N
isN :: NumberPencilBackend -> Bool
isN a = not $ isP a

-- | returns the value embedded in N
getN :: NumberPencilBackend -> Number
getN (N n) = n

-- | returns the value embedded in P
getP :: NumberPencilBackend -> [Number]
getP (P p) = p

-- | similar to Prelude.map
-- applies the fucntion only to the element at given index
-- Note: index starts at 0
mapAtIndex :: (a -> a) -> Int -> [a] -> [a]
mapAtIndex function index list = take index list ++ (function (list !! index)):(drop (index +1) list)

{- | similar to mapAtIndex
applies the function to all given indices
Note: if index given multiple times, it gets applied multiple times

==== __Examples:__

>>> mapAtIndexMultiple (\x -> x+1) [6,6,6] [1..6]
[1,2,3,4,5,9]
>>> mapAtIndexMultiple (\x -> 0) [] [2,2,2]
[2,2,2]
>>> mapAtIndexMultiple (\x -> 5,3,1)
-}
mapAtIndexMultiple :: (a -> a) -> [Int] -> [a] -> [a]
mapAtIndexMultiple f i l = let indices = sort i in help f (nub indices) l where
    help _ [] list = list
    help function [i] list = take i list ++ (function (list !! i)):(drop (i+1) list)
    help function indices@(x:y:xs) list = help function (if x == y then xs else y:xs) (take x list ++ (function (list !! x)):(drop (x+1) list))

{- | takes a list and splits it in lists of given size

==== __Examples:__

>>> split 4 [1..12]
[[1,2,3,4],[5,6,7,8],[9,10,11,12]]
>>> split 0 ['a','b']
[['a','b']]
-}
split :: Int -> [a] -> [[a]]
split 0 list = [list]
split num [] = []
split num list = (take num list):(split num (drop num list))

{- | returns a list of all elements that fulfill the condition

==== __Examples:__

>>> takeIf isP [N One, N Two, P [Three]]
[P [Three]]
>>> takeIf (>6) [1..4]
[]
-}
takeIf :: (a -> Bool) -> [a] -> [a]
takeIf f [] = []
takeIf f (x:xs) = if f x then x:(takeIf f xs) else takeIf f xs

bspFieldN = Field {_number = N Nine, _position = RCB {_row = 1, _column = 2, _box = 1}}
bspFieldP = Field {_number = P [Three, Eight], _position = RCB {_row = 5, _column = 5, _box = 5}}


-- | shorthand used for checking if the input is valid
oneToNine :: [Char]
oneToNine = "123456789"

-- | converts a Char to the custom Number data type
convertCharNumToNumber :: Char -> Number
convertCharNumToNumber a | elem a oneToNine = (toEnum ((read [a])::Int))::Number
                         | otherwise = error $ "Called convertCharNumToNumber with: " ++ [a]

