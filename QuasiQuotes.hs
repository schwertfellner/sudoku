{-# LANGUAGE QuasiQuotes, TemplateHaskell #-}
module QuasiQuotes where

import Language.Haskell.TH.Quote
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Data.Char(isSpace)
import DataTypes

--compile :: String -> Q Exp
compile x = lift (parseGrid x)

parseGrid :: String -> Grid
parseGrid x = let erg = parseSudoku x in Grid erg

parseSudoku :: String -> [Field]
parseSudoku string = help string 0 where
    help (a:as) 80 | as == [] = [(cFCAC a 80)]
    help (a:as) count = (cFCAC a count) : (help as (count + 1))
    help a count | otherwise = error $ "Falsche eingabe bei parseSudoku. \nVerbleibende eingabe: " ++ show a ++ "\nCount stand: " ++ show count

cFCAC :: Char -> Int -> Field
cFCAC char count = if elem char oneToNine
    then Field {_number = (N (convertCharNumToNumber char)), _position = (convertCountToRCB count)}
    else Field {_number = (P [One .. Nine]), _position = (convertCountToRCB count)}

{-instance Lift Field where
    lift (Field {n , p }) = [| Field n p |]
    liftTyped = unsafeTExpCoerce . lift-}

sudoku = QuasiQuoter { quoteExp = compile
                     , quotePat = notHandled "patterns"
                     , quoteType = notHandled "types"
                     , quoteDec = notHandled "declarations"
                     } where notHandled things = error $ things ++ " are not handled by the simple quasiquoter."

