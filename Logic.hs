{-# LANGUAGE TemplateHaskell, RecordWildCards #-}
module Logic where

import DataTypes
import Control.Lens
import Data.List
import System.Directory (createDirectoryIfMissing)
import System.IO
import Control.Monad.State
import Debug.Trace
import QuasiQuotes
import Control.Parallel
--import Graphics.UI.Gtk

makeLenses ''Field
makeLenses ''RCB
{-
instance Eq Grid where
    (l1) == (l2) = help l1 l2 81 where
        help l1 l2 0 = True
        help l1 l2 n = do
            let l1n = map (\x -> view number x) $ filter (\x -> (view position x) == convertCountToRCB n) l1
            let l2n = map (\x -> view number x) $ filter (\x -> (view position x) == convertCountToRCB n) l2
            if l1n == l2n then help l1 l2 (n-1)
            else False
-}

instance Ord Field where
    compare field1 field2 = compare (convertRCBToCount (view position field1)) (convertRCBToCount (view position field2))

{-
tba lOF1 lOF2 count = filter (\x -> (view position x) == convertCountToRCB) $ lOF1 ++ lOF2
-}

-- they are the same (they = lineNotationToBox, boxNotationToLine)
--lineNotationToBox, boxNotationToLine :: [a] -> [a]
lineNotationToBox :: Show a => [a] -> [a]
lineNotationToBox list = help list [] [] [] [] where
    help [] [] [] [] erg | length erg == 81 = erg
    help eingabe l1 l2 l3 erg | length l3 == 9 = help eingabe [] [] [] (erg ++ l1 ++ l2 ++ l3)
    help eingabe l1 l2 l3 erg | length eingabe > 8 = help (drop 9 eingabe) (l1 ++ take 3 eingabe) (l2 ++ take 3 (drop 3 eingabe)) (l3 ++ take 3 (drop 6 eingabe)) erg
    help eingabe l1 l2 l3 erg = error ("Eingabe Liste ist: " ++ show eingabe ++ " l1/l2/l3 sind: " ++ show l1 ++ ", " ++ show l2 ++ ", " ++ show l3 ++ " errechnetes Ergebnis bis zum Fehler: " ++ show erg)

boxNotationToLine :: [a] -> [a]
boxNotationToLine string = concat [ let x = i*3 in (drop x (take (x+3) string)) ++ let y = x + 9 in (drop y (take (y+3) string)) ++ let z = y + 9 in (drop z (take (z+3) string)) | i <- [0,1,2,9,10,11,18,19,20]]

-- | used for testing
-- gives a list of strings "r1c1", "r2c1", etc.
giveIdName :: [Char] -> [[Char]]
giveIdName string = lineNotationToBox ['r':(head (show r)):'c':(head (show c)):string | r <- [1..9], c <- [1..9]]

-- | used for bug fixing
translateStringToGrid :: String -> [Field]
translateStringToGrid string = help string 0 where
    help (a:as) 80 | as == [] = [(createFieldwithCharAndCount a 80)]
    help (a:as) count = (createFieldwithCharAndCount a count) : (help as (count + 1))
    help a count | otherwise = error $ "Falsche eingabe bei translateStringToGrid. \nVerbleibende eingabe: " ++ show a ++ "\nCount stand: " ++ show count

-- | used for bug fixing
createFieldwithCharAndCount :: Char -> Int -> Field
createFieldwithCharAndCount char count = if elem char oneToNine
    then Field {_number = (N (convertCharNumToNumber char)), _position = (convertCountToRCB count)}
    else Field {_number = (P [One .. Nine]), _position = (convertCountToRCB count)}


-- | used for bug fixing
translateGridToString :: [Field] -> [Char]
translateGridToString a = help a [] where
    help [] _ = []
    help ((Field {_number = (N a1), ..}):a2:as) b = help (a2:as) ((fromEnumToChar a1):b)
    help ((Field {_number = (P _), ..}):a2:as) b  = help (a2:as) ('_':b)
    help ((Field {_number = (N a1), ..}):as) b    = reverse ((fromEnumToChar a1):b)
    help ((Field {_number = (P _), ..}):as) b     = reverse ('_':b)

-- | changes the field in the grid according to the given field's coordinates
changeGridNewNumber :: Field -> [Field] -> [Field]
changeGridNewNumber a b = [changePossibleSolutions a i | i <- b]

-- | removes value of a from b if in the same row/column/box as a
changePossibleSolutions :: Field -> Field -> Field
changePossibleSolutions a b = if (isP (view number b)) && ((view (position.row) a == view (position.row) b) || (view (position.column) a == view (position.column) b) || (view (position.box) a == view (position.box) b))
    then set number (P (delete (getN (view number a)) $ getP (view number b))) b
    else b

-- | writes all possible values for every field as pencilmarks (separate from user pencilmarks)
initialGridFilter :: [Field] -> [Field]
initialGridFilter a = help a 0 where
    help a 81 = findHiddenInRCB a 
    help a n  = let b = a !! n in if isP (view number b)
        then help a (n + 1)
        else help (changeGridNewNumber b a) (n + 1)

-- | a naked single in sudoku is a field where only one number is possible
-- if one field only has one value as pencilmark, it must be a naked single
findNakedSingles :: [Field] -> [Field]
findNakedSingles grid = help grid 0 where
    help grid 81 = grid
    help grid n  = let field = grid !! n in if isP (view number field) && (length (getP (view number field)) == 1)
        then let newGrid = mapAtIndex (set number (N (head (getP (view number field))))) n grid in help (changeGridNewNumber (newGrid !! n) newGrid) (n + 1)
        else help grid (n + 1)

-- | shorthand for finding all three types of hidden singles
findHiddenInRCB :: [Field] -> [Field]
--findHiddenInRCB grid = findHiddenSingles (sortRCB Box (findHiddenSingles (findHiddenSingles (sortRCB Column grid) Column) Row)) Box
findHiddenInRCB grid = findHiddenSingles (sortRCB Column (findHiddenSingles (findHiddenSingles (sortRCB Box grid) Box) Row)) Column

-- | a hidden single is a number that can only appear in a specific field within a box/column/row
-- writes all hidden singles according to the row/column or box logic
findHiddenSingles :: [Field] -> RCBEnum -> [Field]
findHiddenSingles grid rcb = let splitGrid = split 9 grid in help 0 splitGrid grid rcb where
    help n (a:as) grid rcb = help (n + 1) as (findHiddenSinglesInStructure n a grid [] rcb) rcb
    help _ [] grid _ = sortRCB Row grid

-- | finds hidden singles in the grid 
findHiddenSinglesInStructure :: Int -> [Field] -> [Field] -> [Number ]-> RCBEnum -> [Field]
findHiddenSinglesInStructure n (a:as) grid candidates rcb = if isP (view number a)
    then findHiddenSinglesInStructure n as grid (getP (view number a) ++ candidates) rcb
    else findHiddenSinglesInStructure n as grid candidates rcb
findHiddenSinglesInStructure n [] grid candidates rcb = if help2 candidates == []
    then grid
    else findHiddenSinglesInStructure n [] (writeHiddenSingle n (head (help2 candidates)) grid rcb) (tail (help2 candidates)) rcb where
        help2 candidates = let nums = map (\x -> (fromEnum x)::Int) candidates in map (\x -> toEnum x::Number) [ n | n <- [1..9] , exactlyOne (\x -> elem n x) (split 1 nums)]

-- | writes the hidden single in a particular spot
writeHiddenSingle :: Int -> Number -> [Field] -> RCBEnum -> [Field]
writeHiddenSingle elementID n grid rcb = help (take 9 (drop (elementID *9) grid)) n grid 0 rcb where
    --help a n _ _ _ = error $ "Inputs:\ncolumn: " ++ show a ++ "\nnumber: " ++ show n
    help (a:as) n grid internalIndex rcb = if isP (view number a) && elem n (getP (view number a))
        then let gridNew = take (elementID*9 + internalIndex) grid ++ [set number (N n) a] ++ drop (elementID*9 + internalIndex +1) grid in sortRCB rcb (changeGridNewNumber (gridNew !! (elementID * 9 + internalIndex)) (sortRCB Row (gridNew)))
        else help as n grid (internalIndex +1) rcb
    help [] n grid _ _ = error "called writeHiddenSingle help w empty list"

{-
writeHiddenSingleM :: Int -> Number -> RCBEnum -> StateT [Field] Identity [Field]
writeHiddenSingleM elementID n rcb = do
    grid <- get
    help (take 9 (drop (elementID *9) grid)) n grid 0 rcb where
        help (a:as) n grid internalIndex rcb = if isP (view number a) && elem n (getP (view number a))
            then let gridNew = take (elementID*9 + internalIndex) grid ++ [set number (N n) a] ++ drop (elementID*9 + internalIndex +1) grid in put $ sortRCB rcb (changeGridNewNumber (gridNew !! (elementID * 9 + internalIndex)) (sortRCB Row (gridNew)))
            else help as n grid (internalIndex +1) rcb
        help [] n grid _ _ = error "called writeHiddenSingle help w empty list"
-}


writeHiddenSingleM :: Number -> StateT [Field] Identity ()
writeHiddenSingleM n = do
    let a field = if isP (view number field) && elem n (getP (view number field)) then set number (N n) field else field --else Fall bleibt das Feld unverändert
    modify $ map (\x -> a x)

-- | shorthand for checking the values of the backend pencilmarks in a field
elemInP :: Number -> Field -> Bool
elemInP e field = elem e $ getP $ view number field

-- | 
findPairs :: [Field] -> [Field]
findPairs grid = help grid [] where
    help [] erg = sortRCB Row erg
    help input erg = help (drop 9 input) (searchElementForPair (take 9 input) erg)


-- |
searchElementForPair :: [Field] -> [Field] -> [Field]
searchElementForPair a b = (help [x | x <- a, isP (view number x)] a) ++ b where
    help :: [Field] -> [Field] -> [Field]
    help a b = let candidates = [checkTuple x y a | x <- [One .. Nine], y <- [One .. Nine]] in if elem True candidates then help2 candidates b else b where
        help2 candidates b | head candidates == False = help2 (tail candidates) b
        help2 [] b = b
        help2 candidates b = let l = 81 - (length candidates) in help2 (tail candidates) (tuplePossibleSolutions (toEnum (mod l 9 + 1)::Number) (toEnum (div l 9 + 1)::Number) b)

-- |
tuplePossibleSolutions :: Number -> Number -> [Field] -> [Field]
tuplePossibleSolutions n1 n2 structure = help n1 n2 structure [] where
    help n1 n2 [] erg     = erg
    help n1 n2 (x:xs) erg = if isP (view number x) && elemInP n1 x then help n1 n2 xs ((set number (P (sort [n1, n2])) x):erg) else help n1 n2 xs (x:erg)

-- |
checkTuple :: Number -> Number -> [Field] -> Bool
checkTuple n1 n2 lField = (n1 /= n2) && ((exactlyN 2 (elemInP n1) lField) && (exactlyN 2 (elemInP n2) lField)) && (exactlyNone (elemInP n2) (dropIfNum n1 lField [])) where
    dropIfNum :: Number -> [Field] -> [Field] -> [Field]
    dropIfNum n [] erg = erg
    dropIfNum n lField erg = if elemInP n (head lField) then dropIfNum n (tail lField) erg else dropIfNum n (tail lField) ((head lField):erg)

-- | uses the hidden & naked single algorithms on the given sudoku (as string) until the sudoku doesn't change
-- the sudoku is either solved or can't be solved with these methods alone
decodeSudoku :: [Char] -> [Char]
decodeSudoku a = let start = initialGridFilter (translateStringToGrid a) in help start [] where
    help a1 a2 | (sortRCB Row a1) == (sortRCB Row a2) = translateGridToString a2
    help a1 _ = help (findHiddenInRCB (findNakedSingles a1)) a1



-- | sorts the given grid according to the given type (row/column/box)
sortRCB :: RCBEnum -> [Field] -> [Field]
sortRCB Row grid = concat [takeIf (\x -> (view (position.row) x) == n) grid | n <- [1..9]]
sortRCB Column grid = concat [takeIf (\x -> (view (position.column) x) == n) grid | n <- [1..9]]
sortRCB Box grid = concat [takeIf (\x -> (view (position.box) x) == n) grid | n <- [1..9]]

-- | 
-- this is strongly based on: https://stackoverflow.com/questions/58682357/how-to-create-a-file-and-its-parent-directories-in-haskell
-- saveString :: String -> IO ()
saveString string = do 
    createDirectoryIfMissing True "src"
    handle <- openFile "src/saved-games.txt" ReadWriteMode
    savedGames <- hGetContents handle
    let countsaved = if savedGames == [] then "1" else show (length (lines savedGames) + 1)
    putStrLn countsaved
    hClose handle
    appendFile "src/saved-games.txt" (countsaved ++ ":" ++ string ++ "\n")

-- | used for debugging
-- prints the given sudoku (as string) to the terminal in a readable format
printSudokuStringNice :: [Char] -> IO ()
printSudokuStringNice a = help a [] 1 where
    help [] b _ = putStrLn $ reverse (drop 1 b)
    help (a:as) b 9 = help as ('\n':a:b) 1
    help (a:as) b n = help as (' ':a:b) (n + 1)


bsp5NakedSingles = "5_1374___429_857_3378_6241_83___6_577_5__8_9_19______4_5_____319___3_______8____9" -- Easy Difficulty, can be solved by naked singles alone (Sudoku.com)
--                 "56137492842918576337896241583__96157745_183961967532842576_983198_5316726138_75_9"
bspEvil = "___3_____8_______6__5_194_______2_____1_759___9_____4____2__7____34_____5___37_8_" -- Evil Difficulty, 1 naked single (Sudoku.com)
bspEvilGrid = initialGridFilter (translateStringToGrid bspEvil)
bsp1 = findHiddenSingles (sortRCB Column bspEvilGrid) Column
bspMedium = "75_19_2___1____3___9______7_27___5_9_____78____1__4_7_9__3784___74__1___185____6_" -- Medium Difficulty, (Sudoku.com)
bspHard = "______18___65____3__4_1__5__9_2____4_8_1______7__3_9_8___________23____9_4___7__5"
bspExpert = "6725__________________49____1_____7___3_854__9_____56____2_____2__7_3_9__5____83_"
--bsp2 = [sudoku|2__4__3_85_____42_47392_____________812_____5__4____3_7___4__1_____7__849__83__5_|]




testDecode :: String -> String
testDecode a = let start = initialGridFilter (translateStringToGrid a) in translateGridToString (snd (runState test start)) where
    test = do
        initialGridFilterM
        a <- get
        --traceM $ "tracing with showGrid:\n" ++ (showGrid a) ++ "\nend tracing with showGrid!"
        --traceM $ "tracing with execState:\n" ++ (execState (translateGridToString a) a) ++ "\nend tracing with execState!"
        --traceM $ "tracing with evalState:\n" ++ (evalState (translateGridToString a) a) ++ "\nend tracing with evalState!"
        -- (evalState (test4Equality startState) oldState)
        --print stateToString 
        --trace ("calling recusion\n") (recursion)
        --readState3
        recursion

{-recursion old = do
    val <- get
    if val == old 
        then modify id
        else do
            modify findNakedSingles
            modify findHiddenInRCB
            recursion val

fHS rcb = do grid <- get
    let splitGrid = split 9 grid in help 0 splitGrid grid rcb where
    help n (a:as) grid rcb = help (n + 1) as (fHSInStructure n a grid [] rcb) rcb
    help _ [] grid _ = sortRCB Row grid

fHSInStructure :: Int -> [Field] -> [Field] -> [Number ]-> RCBEnum -> [Field]
fHSInStructure n (a:as) grid candidates rcb = if isP (view number a)
    then fHSInStructure n as grid (getP (view number a) ++ candidates) rcb
    else fHSInStructure n as grid candidates rcb
fHSInStructure n [] grid candidates rcb = if help2 candidates == []
    then grid
    else fHSInStructure n [] (writeHiddenSingle n (head (help2 candidates)) grid rcb) (tail (help2 candidates)) rcb where
        help2 candidates = let nums = map (\x -> (fromEnum x)::Int) candidates in map (\x -> toEnum x::Number) [ n | n <- [1..9] , exactlyOne (\x -> elem n x) (split 1 nums)]
-}

{-
recursion :: StateT [Field] Identity ()
recursion = do
    val <- get
    
    modify findNakedSingles
    modify findHiddenInRCB
    newVal <- get
    if (show val == show newVal)
        then return ()
        else trace ("calling recusion\n" ++ (show newVal)) (recursion)

-}

recursion :: StateT [Field] Identity ()
recursion = do
    val <- get
    let r1 = findNakedSingles val
    let r2 = findHiddenInRCB val
    let erg = r1 `par` r2 `pseq` minGrid r1 r2
    put erg
    newVal <- get
    if (show val == show newVal)
        then return ()
        else recursion--trace ("calling recusion\n" ++ (show newVal)) (recursion)

{-
recursion1 = do
    s <- get
    helpRecursion s [] where
        --helpRecursion :: [Field] -> [Field] -> m ()
        helpRecursion s save = if s == save 
            then return ()
            else do
                modify findNakedSingles
                modify findHiddenInRCB
                newS <- get
                helpRecursion newS s-}

{-
testRecursion = helpRecursion [] where
    helpRecursion :: [Field] -> [Field]
    helpRecursion save = do
        let currentGrid = [] -- <- get
        if currentGrid == save
            then currentGrid
            else do
                t <- get
                --modify findNakedSingles
                helpRecursion []
-}
{-
readState :: StateT [Field] Identity [Field]
readState = do
    currentState <- get
    return currentState


readState2 :: StateT [Field] Identity ()
readState2 = do
    currentState <- get
    let findInState = modify iNeedThisToTest
    return $ evalState findInState currentState
-}

iNeedThisToTest :: [Field] -> [Field]
iNeedThisToTest (x:xs) = ((over number (liftToN.succ.getN) x):xs) where
    liftToN a = N a

test4Equality :: [Field] -> [Field] -> Bool
test4Equality lOF1 lOF2 = (sort lOF1) == (sort lOF2)

tempFun :: [Field] -> [Field] -> Bool
tempFun (x:_) _ = getN (view number x) == One

{-
recursionTest1001 oldState = do
    startState <- get
    if (evalState (tempFun startState oldState) oldState)     -- (evalState (test4Equality startState) oldState)
        then return ()
        else do
            modify iNeedThisToTest


recursionTest1002 [] = do
    grid <- get
    return grid
recursionTest1002 list = do


recursionTest1003 = do
    startState <- get
    if tempFun startState
-}


equal [] [] = True
equal a [] = error (show a)
equal [] b = error (show b)
equal (a:as) (b:bs) = if a == b then equal as bs else False



readState3 :: StateT [Field] Identity [Field]
readState3 = do
    currentState <- get
    modify iNeedThisToTest
    alteredState <- get
    return alteredState


--initialGridFilterM :: [Field] -> [Field]
initialGridFilterM :: StateT [Field] Identity ()
initialGridFilterM = do
    a <- get
    help a 0 where
        help a 81 = do
            y <- get
            put y 
        help a n  = let b = a !! n in if isP (view number b)
            then help a (n + 1)
            else do
                changeGridNewNumberM b
                x <- get
                help x (n + 1)


-- | changes the field in the grid according to the given field's coordinates
-- changeGridNewNumberM :: Field -> [Field] -> [Field]
changeGridNewNumberM :: Field -> StateT [Field] Identity ()
changeGridNewNumberM a = do
    x <- get 
    put $ [changePossibleSolutions a i | i <- x]

--changeGridNewNumberM2 a = modify (\x -> map changePossibleSolutions a x)
{-
stateToString :: StateT [Field] Identity ()
stateToString = do
    a <- get
    return $ evalState id a-}
    
--findHiddenSinglesInStructureM :: Int -> [Field] -> [Field] -> [Number ]-> RCBEnum -> [Field]
{-findHiddenSinglesInStructureM n (a:as) candidates rcb = do
    grid <- get
    if isP (view number a)
        then findHiddenSinglesInStructureM n as grid (getP (view number a) ++ candidates) rcb
        else findHiddenSinglesInStructureM n as grid candidates rcb
findHiddenSinglesInStructureM n [] grid candidates rcb = if help2 candidates == []
    then grid
    else findHiddenSinglesInStructureM n [] (writeHiddenSingle n (head (help2 candidates)) grid rcb) (tail (help2 candidates)) rcb where
        help2 candidates = let nums = map (\x -> (fromEnum x)::Int) candidates in map (\x -> toEnum x::Number) [ n | n <- [1..9] , exactlyOne (\x -> elem n x) (split 1 nums)]-}

showGrid [] = []
showGrid (a:as) = (show a) ++ showGrid as


minGrid :: [Field] -> [Field] -> [Field]
minGrid [] [] = []
minGrid (a:as) (b:bs) = if compare a b == EQ
    then (helpMinGrid a b):(minGrid as bs)
    else error $ "minGrid: something went wrong in the sorting\na: " ++ show a ++ "\nb: " ++ show b

helpMinGrid f1 f2 | isN (view number f1) = f1
                  | isN (view number f2) = f2
                  | otherwise = over number (\x -> P (intersect (getP (view number f2)) (getP x))) f1

--testQQ x = sudoku x
{-main = do
    let a = replicate 100 bsp5NakedSingles
    let erg = threadedList a
    write1 erg-}

write1 [] = print 2
write1 a | head a == head (tail a) = do
    write1 (tail a)
    print 1
write1 a | otherwise = print 0

threadedList a = help a (length a) where
    help _ 0 = []
    help x n = let f = testDecode (head x)
                   h = (:) f []
                   t = help (tail x) (n - 1)
        in f `par` t `pseq` h : t

{-threadedList a = help a (length a) where
    help _ 0 = []
    help x n = map testDecode x-}